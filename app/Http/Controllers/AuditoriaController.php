<?php

namespace App\Http\Controllers;

use App\AuditoriaModel;
use Illuminate\Http\Request;
use Validator;
use Hash;
use Illuminate\Support\Facades\DB;
use Mail;
use Redirect;
use Session;

use SecurityLib\Strength;
use Illuminate\Support\Arr;

class AuditoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arr = AuditoriaModel::all(['id','plaza', 'placa', 'fecha_ingreso', 'fecha_salida']);
        return json_encode($arr);

    }

//    public function Randon(){
//
//        $InicialProvincias = 'ABCEGILMNOPSTZ';
//        $length = 3;
//        $placa_letras = substr(str_shuffle(str_repeat($InicialProvincias, $length)), 0, $length);
//        $Numeros = '1234567890';
//        $length = 4;
//        $placa_numeros = substr(str_shuffle(str_repeat($Numeros, $length)), 0, $length);
//        $placa = $placa_letras . "-" . $placa_numeros;
//        return dd($placa);
//
//    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //return $request;

        try {
            DB::beginTransaction();
            $InicialProvincias = 'ABCEGILMNOPSTZ';
            $length = 3;
            $placa_letras = substr(str_shuffle(str_repeat($InicialProvincias, $length)), 0, $length);
            $Numeros = '1234567890';
            $length = 4;
            $placa_numeros = substr(str_shuffle(str_repeat($Numeros, $length)), 0, $length);
            $placa = $placa_letras . "-" . $placa_numeros;
            $auditoria_guardar = new AuditoriaModel();
            $auditoria_guardar->plaza=$request->plaza;
            $auditoria_guardar->placa=$placa;
            $auditoria_guardar->fecha_ingreso=$request->fecha_ingreso;
            $auditoria_guardar->fecha_salida=$request->fecha_salida;
            $auditoria_guardar->save();
            DB::commit();
            return 1;

        } catch (\Illuminate\Database\QueryException $e) {
            return $e->getMessage();
            DB::rollBack();
            return 10;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AuditoriaModel  $auditoriaModel
     * @return \Illuminate\Http\Response
     */
    public function show(AuditoriaModel $auditoriaModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AuditoriaModel  $auditoriaModel
     * @return \Illuminate\Http\Response
     */
    public function edit(AuditoriaModel $auditoriaModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AuditoriaModel  $auditoriaModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AuditoriaModel $auditoriaModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AuditoriaModel  $auditoriaModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(AuditoriaModel $auditoriaModel)
    {
        //
    }
}
