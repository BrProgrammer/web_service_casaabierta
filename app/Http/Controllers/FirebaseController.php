<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;


class FirebaseController extends Controller
{
    //



    public function index(){

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://casaabiertafacci-d00bb.firebaseio.com/')
            ->create();

        $database = $firebase->getDatabase();
        $ref = $database->getReference("Puestos");
        $puestos = $ref->getValue();
        foreach ($puestos as $puestos){
            $all_puestos[]= $puestos;
        }
        return json_encode($all_puestos);
    }

    public function store(Request $request){

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://casaabiertafacci-d00bb.firebaseio.com/')
            ->create();

        $database = $firebase->getDatabase();
        $ref = $database->getReference("Puestos");
        $ref->getChild($request->id)->set(
            [
                'nombre' =>$request->nombre,
                'estado' =>$request->estado,
                'id' =>$request->id
            ]
        );
        return 1;
    }

    public function update($id, Request $request){
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://casaabiertafacci-d00bb.firebaseio.com/')
            ->create();
        $database = $firebase->getDatabase();
        $ref = $database->getReference("Puestos");
        if ($request->estado=="1"){
            $imagen = "https://firebasestorage.googleapis.com/v0/b/casaabiertafacci-d00bb.appspot.com/o/kisspng-car-clip-art-red-top-car-png-5ab18cf22f56b9.7533253315215853941939.png?alt=media&token=5a76de3e-cc54-4d95-8311-5b000641a1bd";
        }else{
            $imagen = "https://firebasestorage.googleapis.com/v0/b/casaabiertafacci-d00bb.appspot.com/o/d3jme6i-8c702ad4-4b7a-4763-9901-99f8b4f038b0.png?alt=media&token=df988a6f-054f-45d9-b8e2-999568ecbc95";
        }
        $ref->getChild($id)->update(
            [
                'estado' =>$request->estado,
                'imagen' =>$imagen
            ]
        );
    }

}
